from django.conf.global_settings import AUTH_USER_MODEL
from django.db import models


class Name(models.Model):
    author = models.ForeignKey(AUTH_USER_MODEL, default=None, null=True)
    date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=256, unique=True)
    reason = models.TextField(max_length=512)

    def __str__(self):
        return f"{self.name}: {self.reason}"
