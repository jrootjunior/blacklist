from django import http
from django import views

from . import models


class BlackListView(views.View):
    def get(self, request):
        queryset = models.Name.objects.all()
        return http.JsonResponse(data={'result': [
            {
                'name': name.name,
                'reason': name.reason,
                'author': str(name.author)
            } for name in queryset]
        })


black_list_view = BlackListView.as_view()
