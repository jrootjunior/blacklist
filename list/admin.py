from django.contrib import admin

from . import models


class NameAdmin(admin.ModelAdmin):
    fields = ('name', 'reason')
    list_display = ('name', 'reason', 'author', 'date')

    list_filter = ('author', 'reason', 'date')

    def save_model(self, request, obj, form, change):
        if obj.author is None:
            obj.author = request.user
        return super(NameAdmin, self).save_model(request, obj, form, change)


admin.site.register(models.Name, NameAdmin)
